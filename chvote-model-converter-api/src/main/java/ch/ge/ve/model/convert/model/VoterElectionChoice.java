/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.model;

import ch.ge.ve.protocol.model.Election;

/**
 * A specialised {@link VoterChoice} that contains additional metadata for an election choice.
 */
public class VoterElectionChoice extends VoterChoice {
  private final String  electoralRollId;
  private final boolean isMajorityElection;
  private final boolean isProportionalElection;
  private final boolean isElectoralRoll;
  private final boolean isEmptyCandidate;
  private final boolean isEmptyList;
  private final boolean isBlankBallot;

  public VoterElectionChoice(Election election,
                             String voteId,
                             String candidateId,
                             String electoralRollId,
                             boolean isMajorityElection,
                             boolean isProportionalElection,
                             boolean isElectoralRoll,
                             boolean isEmptyCandidate,
                             boolean isEmptyList,
                             boolean isBlankBallot) {
    super(election, voteId, candidateId);

    this.electoralRollId = electoralRollId;
    this.isMajorityElection = isMajorityElection;
    this.isProportionalElection = isProportionalElection;
    this.isElectoralRoll = isElectoralRoll;
    this.isEmptyCandidate = isEmptyCandidate;
    this.isEmptyList = isEmptyList;
    this.isBlankBallot = isBlankBallot;
  }

  public String getElectoralRollId() {
    return electoralRollId;
  }

  public boolean isMajorityElection() {
    return isMajorityElection;
  }

  public boolean isProportionalElection() {
    return isProportionalElection;
  }

  public boolean isElectoralRoll() {
    return isElectoralRoll;
  }

  public boolean isEmptyCandidate() {
    return isEmptyCandidate;
  }

  public boolean isEmptyList() {
    return isEmptyList;
  }

  public boolean isBlankBallot() {
    return isBlankBallot;
  }
}
