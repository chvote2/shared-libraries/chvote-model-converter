/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.api

import ch.ge.ve.model.convert.model.ElectionDefinition
import ch.ge.ve.protocol.model.Candidate
import ch.ge.ve.protocol.model.DomainOfInfluence
import ch.ge.ve.protocol.model.Election
import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey
import spock.lang.Specification

class ElectionSetBuilderTest extends Specification {

  private static final DomainOfInfluence DOI = new DomainOfInfluence("doi")
  private static final PrintingAuthorityWithPublicKey PRINTING_AUTHORITY =
          new PrintingAuthorityWithPublicKey("pa1", BigInteger.ONE)

  ElectionSetBuilder builder

  void setup() {
    builder = new ElectionSetBuilder()
  }

  def "the builder should fail when no election definition has been set"() {
    when: "the build method is called and no election definition has been set"
    builder.build()

    then:
    thrown(IllegalStateException)
  }

  def "the builder should reject incoherent election definitions (not enough candidates)"() {
    given: "an incoherent election definition (not enough candidates)"
    def election = new Election("EL", 42, 1, DOI)
    def electionDefinition = new ElectionDefinition(election, [new Candidate("candidate1")])

    when:
    builder.addElectionDefinitions([electionDefinition])

    then:
    thrown(IllegalArgumentException)
  }

  def "the builder should reject incoherent election definitions (candidates >= selections)"() {
    given: "an incoherent election definition (not enough candidates)"
    def election = new Election("EL", 1, 1, DOI)
    def electionDefinition = new ElectionDefinition(election, [new Candidate("candidate1")])

    when:
    builder.addElectionDefinitions([electionDefinition])

    then:
    thrown(IllegalArgumentException)
  }

  def "the builder should fail when no printing authorities have been set"() {
    given: "an election definition is set"
    def election = new Election("EL", 3, 1, DOI)
    def candidates = [new Candidate("candidate1"), new Candidate("candidate2"), new Candidate("candidate3")]
    def electionDefinition = new ElectionDefinition(election, candidates)
    builder.addElectionDefinitions([electionDefinition])

    when: "the build method is called and no printing authorities have been set"
    builder.build()

    then:
    thrown(IllegalStateException)
  }

  def "the builder should fail when no voter count has been set"() {
    given: "an election definition is set"
    def election = new Election("EL", 3, 1, DOI)
    def candidates = [new Candidate("candidate1"), new Candidate("candidate2"), new Candidate("candidate3")]
    def electionDefinition = new ElectionDefinition(election, candidates)
    builder.addElectionDefinitions([electionDefinition])

    and: "a printing authority is set"
    builder.addPrintingAuthorities([PRINTING_AUTHORITY])

    when: "the build method is called and no voter count has been set"
    builder.build()

    then:
    thrown(IllegalStateException)
  }

  def "the builder should fail when no counting circle count has been set"() {
    given: "an election definition is set"
    def election = new Election("EL", 3, 1, DOI)
    def candidates = [new Candidate("candidate1"), new Candidate("candidate2"), new Candidate("candidate3")]
    def electionDefinition = new ElectionDefinition(election, candidates)
    builder.addElectionDefinitions([electionDefinition])

    and: "a printing authority is set"
    builder.addPrintingAuthorities([PRINTING_AUTHORITY])

    and: "the voter count has been set"
    builder.setVoterCount(1L)

    when: "the build method is called and no counting circle count has been set"
    builder.build()

    then:
    thrown(IllegalStateException)
  }

  def "the builder should succeed if all required elements have been set"() {
    given: "an election definition is set"
    def election = new Election("EL", 3, 1, DOI)
    def candidates = [new Candidate("candidate1"), new Candidate("candidate2"), new Candidate("candidate3")]
    def electionDefinition = new ElectionDefinition(election, candidates)
    builder.addElectionDefinitions([electionDefinition])

    and: "a printing authority is set"
    builder.addPrintingAuthorities([PRINTING_AUTHORITY])

    and: "the voter count has been set"
    builder.setVoterCount(1L)

    and: "the counting circle count has been set"
    builder.setCountingCircleCount(1)

    when: "the build method is called"
    def electionSetWithCerts = builder.build()

    then: "the corresponding election set should be returned"
    electionSetWithCerts.voterCount == 1L
    electionSetWithCerts.elections == [election]
    electionSetWithCerts.candidates == candidates
    electionSetWithCerts.printingAuthorities == [PRINTING_AUTHORITY]
  }
}
