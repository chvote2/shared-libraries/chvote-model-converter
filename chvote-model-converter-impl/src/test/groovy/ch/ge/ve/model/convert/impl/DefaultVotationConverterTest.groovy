/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl

import ch.ge.ve.protocol.model.Candidate
import ch.ge.ve.protocol.model.DomainOfInfluence
import spock.lang.Specification

class DefaultVotationConverterTest extends Specification {
  DefaultVotationConverter converter

  void setup() {
    converter = new DefaultVotationConverter()
  }

  def "it should successfully convert an eCH-0159 xml stream"() {
    given: "a valid eCH-0159 xml stream"
    def resource = DefaultVoterConverterTest.classLoader.getResourceAsStream("eCH-0159.xml")

    when: "converting the input stream"
    def electionDefinitions = converter.convertToElectionDefinitionList(resource)

    then: "the election definitions should not be null"
    electionDefinitions != null

    and: "the result should have the expected size"
    electionDefinitions.size() == 3

    and: "the question identification should be copied to the election identifier"
    electionDefinitions[0].election.identifier == "FED_CH_Q1"
    electionDefinitions[1].election.identifier == "CAN_GE_Q1"
    electionDefinitions[2].election.identifier == "COM_6608_Q1"

    and: "the number of selections should always be 1"
    electionDefinitions.findAll { it.election.numberOfSelections == 1 }.size() == 3

    and: "the declared number of candidates should be as expected"
    electionDefinitions[0].election.numberOfCandidates == 3
    electionDefinitions[1].election.numberOfCandidates == 3
    electionDefinitions[2].election.numberOfCandidates == 3

    and: "the domain of influence should be the expected one"
    electionDefinitions[0].election.applicableDomainOfInfluence == new DomainOfInfluence("1")
    electionDefinitions[1].election.applicableDomainOfInfluence == new DomainOfInfluence("3")
    electionDefinitions[2].election.applicableDomainOfInfluence == new DomainOfInfluence("6608")

    and: "the number of candidates should be as expected"
    electionDefinitions[0].candidates.size() == 3
    electionDefinitions[1].candidates.size() == 3
    electionDefinitions[2].candidates.size() == 3

    and: "the 'candidates' should be the ones expected"
    electionDefinitions[0].candidates.containsAll([
            new Candidate("FED_CH_Q1:YES"),
            new Candidate("FED_CH_Q1:NO"),
            new Candidate("FED_CH_Q1:BLANK")])
    electionDefinitions[1].candidates.containsAll([
            new Candidate("CAN_GE_Q1:YES"),
            new Candidate("CAN_GE_Q1:NO"),
            new Candidate("CAN_GE_Q1:BLANK")])
    electionDefinitions[2].candidates.containsAll([
            new Candidate("COM_6608_Q1:YES"),
            new Candidate("COM_6608_Q1:NO"),
            new Candidate("COM_6608_Q1:BLANK")])
  }
}
