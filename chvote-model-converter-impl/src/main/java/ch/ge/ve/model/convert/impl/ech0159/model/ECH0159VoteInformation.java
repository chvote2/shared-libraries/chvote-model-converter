/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl.ech0159.model;

import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery.VoteInformation;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An {@link VoteInformation} decorator.
 */
public class ECH0159VoteInformation {
  private final VoteInformation     voteInformation;
  private final List<ECH0159Ballot> ballots;

  /**
   * Create a new eCH-0159 vote information decorator.
   *
   * @param voteInformation the {@link VoteInformation} to decorate.
   */
  public ECH0159VoteInformation(VoteInformation voteInformation) {
    this.voteInformation = voteInformation;
    this.ballots = voteInformation.getBallot().stream().map(ECH0159Ballot::new).collect(Collectors.toList());
  }

  /**
   * Get the identification string of the decorated {@link VoteInformation}.
   * Same as calling {@code voteInformation.getVote().getVoteIdentification()}.
   *
   * @return the vote identification string.
   */
  public String getVoteIdentification() {
    return voteInformation.getVote().getVoteIdentification();
  }

  /**
   * Get the list of ballots contained in the decorate {@link VoteInformation}. The ballots are in turn decorated by
   * {@link ECH0159Ballot}.
   *
   * @return the list of decorated ballots.
   */
  public List<ECH0159Ballot> getBallots() {
    return ballots;
  }

  /**
   * Whether there is a ballot in this vote that contains a question with given question identifier.
   *
   * @param identifier the question identifier.
   *
   * @return true if there is a ballot that contains a question with the given identifier, false otherwise.
   */
  public boolean hasQuestionIdentifier(String identifier) {
    return ballots.stream().filter(ballot -> ballot.hasQuestionIdentifier(identifier)).count() > 0;
  }
}
