/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - chvote-model-converter                                                                         -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.model.convert.impl;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import org.bouncycastle.crypto.digests.Blake2bDigest;

/**
 * Voter ID generator.
 */
public final class VoterIdGenerator {
  /**
   * Generates a 36-characters long voter identifier from the given inputs.
   *
   * @param deliveryId the delivery identifier.
   * @param registerId the register identifier.
   * @param personId   the person identifier.
   *
   * @return the generated voter identifier.
   */
  public String generateId(String deliveryId, String registerId, String personId) {
    byte[] out = new byte[32];
    byte[] id = (deliveryId + registerId + personId).getBytes(StandardCharsets.UTF_8);
    Blake2bDigest blake2 = new Blake2bDigest(256);
    blake2.update(id, 0, id.length);
    blake2.doFinal(out, 0);
    return Base64.getUrlEncoder().encodeToString(Arrays.copyOf(out, 27));
  }
}
